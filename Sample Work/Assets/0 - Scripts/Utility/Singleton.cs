﻿using UnityEngine;

namespace MCWork
{

    /// <summary>
    /// Evil but if you're going to use them they may as well be consistent.
    /// </summary>
    /// <typeparam name="T">The concrete singleton type.</typeparam>
    public class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        
        /// <summary>
        /// Should we be able to access the singleton via static instantiation instead of expecting a manager in a scene(singleton manager)
        /// we can assign an instantiator in the client static constructor which will be called when the need arises.
        /// </summary>
        public static System.Action DoInstantiate;

        private static T _instance;

        /// <summary>
        /// Public instance access.
        /// </summary>
        public static T Instance
        {
            get
            {
                if (_instance == null && DoInstantiate != null)
                {
                    DoInstantiate();
                }

                return _instance;
            }
        }

        protected virtual void Awake()
        {
            if (_instance != null)
            {
                Debug.LogErrorFormat("More than one instance of type {0} exists", typeof(T));
                return;
            }

            _instance = this as T;
        }

        private void OnDestroy()
        {
            _instance = null;
        }
    }

}
