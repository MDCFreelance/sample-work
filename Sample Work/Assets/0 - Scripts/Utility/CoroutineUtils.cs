﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MCWork
{

    /// <summary>
    /// Useful coroutine functionalities.
    /// </summary>
    public class CoroutineUtils : Singleton<CoroutineUtils>
    {

        public delegate bool Interpolator(float f);

        /// <summary>
        /// We provide an instantiation callback to the singleton pattern here so that static interface usage instantiates if not already done.
        /// </summary>
        static CoroutineUtils()
        {
            DoInstantiate = HandleInstatiate;
        }


        /// <summary>
        /// Static coroutine starter.  Very convenient.
        /// I find it difficult to trust coroutines executing on random monobehaviours.  I prefer to centralize them in a place where I know client
        /// code isn't indiscriminantly stopping all coroutines.
        /// </summary>
        /// <param name="routine">The routine we wish to handle globally.</param>
        /// <returns>The routine handle</returns>
        public static Coroutine BeginCoroutine(IEnumerator routine)
        {
            return CoroutineUtils.Instance.StartCoroutine(routine);
        }

        /// <summary>
        /// And a coroutine - must ahve been started via BeginCoroutine (or on the singleton monobehaviour instance in some other way)
        /// </summary>
        /// <param name="routine">The coroutine to end.</param>
        public static void EndCoroutine(ref Coroutine routine)
        {
            CoroutineUtils.Instance.StopCoroutine(routine);
            //Be nice and set to null - I find myself setting them to null after the fact anyway
            routine = null;
        }

        /// <summary>
        /// Handles instantiation and component setup should the static interface be used.
        /// </summary>
        private static void HandleInstatiate()
        {
            GameObject instance = new GameObject("COROUTINE_UTILS");
            instance.AddComponent<CoroutineUtils>();
        }

        /// <summary>
        /// Interpolation helper.
        /// </summary>
        /// <param name="duration">Length of interp procedure</param>
        /// <param name="step">Each step of the interpolation. The completion [0, 1] is passed to the delegate. Return false to terminate early, else return true.</param>
        /// <param name="onFinished">Callback on interpolation completion.</param>
        public static IEnumerator DoInterpolation(float duration, Interpolator step, System.Action onFinished = null)
        {
            if (step == null)
            {
                Debug.LogError("step is null.");
                yield break;
            }

            float time = 0f;
            while (!Mathf.Approximately(duration, time))
            {
                time = Mathf.Clamp(time + Time.deltaTime, 0f, duration);
                float t = time / duration;
                if (!step(t))
                    yield break;
                yield return null;
            }

            if (onFinished != null)
                onFinished();
        }

        /// <summary>
        /// Interpolation yield instruction.
        /// </summary>
        public class WaitInterpolation : CustomYieldInstruction
        {
            float time = 0f;
            float duration = 0f;
            Interpolator step;
            float start, end;

            public WaitInterpolation(float duration, float start, float end, Interpolator step)
            {
                this.start = start;
                this.end = end;
                this.duration = duration;
                this.step = step;
            }

            public override bool keepWaiting
            {
                get
                {
                    time = Mathf.Clamp(time + Time.deltaTime, 0f, duration);
                    float t = time / duration;
                    if (!step(Mathfx.Hermite(start, end, t)))
                        return false;
                    return !Mathf.Approximately(duration, time);
                }
            }
        }
       
        /// <summary>
        /// Direct interpolation from start to end.  Interpolator provided values per step along the [start,end] range.
        /// </summary>
        /// <param name="duration">How long you wish the operation to take to complete.</param>
        /// <param name="start">Initial value.</param>
        /// <param name="end">Ending value</param>
        /// <param name="step">Inteprolator, return false to early out from the procedure.</param>
        public static IEnumerator DoInterpolation(float duration, float start, float end, Interpolator step)
        {
            if (step == null)
            {
                Debug.LogError("step is null.");
                yield break;
            }

            float time = 0f;
            while (!Mathf.Approximately(duration, time))
            {
                time = Mathf.Clamp(time + Time.deltaTime, 0f, duration);
                float t = time / duration;
                if (!step(Mathf.Lerp(start, end, t)))
                    yield break;
                yield return null;
            }
        }

        /// <summary>
        /// Simple delay - fire something in X seconds
        /// </summary>
        /// <param name="delay">The wait time, in seconds</param>
        /// <param name="action">The action to complete when the delay is finished.</param>
        public static IEnumerator Delay(float delay, System.Action action)
        {
            if (action == null)
                throw new System.ArgumentException("action argument can't be null");
            yield return new WaitForSeconds(delay);
            action();
        }


        /// <summary>
        /// Delay to end of frame helper.
        /// </summary>
        /// <param name="action">The action to complete when the delay is finished.</param>
        public static IEnumerator DelayUnityFrameEnd(System.Action action)
        {
            if (action == null)
                throw new System.ArgumentException("action argument can't be null");
            yield return new WaitForEndOfFrame();
            action();
        }

        /// <summary>
        /// A simple delay which can be yielded on within another coroutine. Optional input based interrupt
        /// </summary>
        public static IEnumerator WaitForSeconds(float seconds, bool interruptable)
        {
            float time = seconds;
            while (time > 0f)
            {
                time -= Time.deltaTime;
                if (interruptable && (Input.anyKey || Input.GetMouseButton(0) || Input.GetMouseButton(1)))
                {
                    yield break;
                }
                yield return null;
            }
        }
    }


}
