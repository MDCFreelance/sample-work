﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace MCWork
{
    [CustomEditor(typeof(SoundAssetPlayer))]
    public class SoundAssetPlayerInspector : Editor
    {

        SoundAssetPlayer player;
        AudioSource previewObj;

        public override void OnInspectorGUI()
        {
            player = target as SoundAssetPlayer;
            base.OnInspectorGUI();

            if (player.Asset != null && GUILayout.Button("Test") && previewObj == null)
            {
                GameObject prev = new GameObject("SOUNDASSET_INSPECTOR_PREVIEW");
                previewObj = prev.AddComponent<AudioSource>();
                previewObj.clip = player.Asset.GetRandom();
                previewObj.pitch = player.Asset.RandomPitch;
                previewObj.volume = player.Asset.RandomVolume;
                previewObj.Play();

                //PlayClip();
                if (EditorApplication.update != Update)
                    EditorApplication.update += Update;
            }
        }

        void Update()
        {
            if (previewObj == null)
                return;

            if (!previewObj.isPlaying)
            {
                GameObject.DestroyImmediate(previewObj.gameObject);
                previewObj = null;
            }
        }

        private void OnDisable()
        {
            EditorApplication.update -= Update;
            if (previewObj != null)
            {
                GameObject.DestroyImmediate(previewObj);
                previewObj = null;
            }
        }
    }

}