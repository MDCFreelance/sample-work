﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;

namespace MCWork
{

    [CustomEditor(typeof(SoundAsset))]
    public class SoundAssetInspector : Editor
    {
        private SoundAsset asset;
        private AudioSource previewObj;
        private SerializedProperty clips;

        private void OnEnable()
        {
            clips = serializedObject.FindProperty("clips");
        }

        private void OnDisable()
        {
            EditorApplication.update -= Update;
            if (previewObj != null)
            {
                GameObject.DestroyImmediate(previewObj);
                previewObj = null;
            }
        }

        public override void OnInspectorGUI()
        {
            if (asset == null)
            {
                asset = target as SoundAsset;
            }
            EditorGUI.BeginChangeCheck();

            EditorGUILayout.PropertyField(clips, true);
            EditorGUILayout.MinMaxSlider("Pitch", ref asset.minPitch, ref asset.maxPitch, 0, 2);
            EditorGUILayout.MinMaxSlider("Volume", ref asset.minVolume, ref asset.maxVolume, 0, 2);

            if (GUILayout.Button("Test") && previewObj == null)
            {
                GameObject prev = new GameObject("SOUNDASSET_INSPECTOR_PREVIEW");
                previewObj = prev.AddComponent<AudioSource>();
                previewObj.clip = asset.GetRandom();
                previewObj.pitch = asset.RandomPitch;
                previewObj.volume = asset.RandomVolume;
                previewObj.Play();

                //PlayClip();
                if (EditorApplication.update != Update)
                    EditorApplication.update += Update;
            }

            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(target);
                serializedObject.ApplyModifiedProperties();
            }
        }

        void Update()
        {
            if (previewObj == null)
                return;

            if (!previewObj.isPlaying)
            {
                GameObject.DestroyImmediate(previewObj.gameObject);
                previewObj = null;
            }
        }
    }
}