﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

namespace MCWork
{

    /// <summary>
    /// Sound asset handling. Allows for multiple clips as part of a single "sound" concept.
    /// </summary>
    [CreateAssetMenu(fileName = "Sound Asset", menuName = "Sound Asset")]
    public class SoundAsset : ScriptableObject
    {

        [SerializeField]
        AudioClip[] clips;

        [SerializeField]
        public float minPitch = 1;
        [SerializeField]
        public float maxPitch = 1;

        [SerializeField]
        public float minVolume = 1;
        [SerializeField]
        public float maxVolume = 1;

        private int lastClip = 0;

        public AudioClip GetRandom()
        {
            if (clips.Length < 1)
                return null;
            int change = Random.Range(1, clips.Length - 1);
            int max = Mathf.Max(clips.Length - 1, 1);

            //        Debug.LogFormat("{0} = {0} + {1} % {2}", lastClip, change, max);
            lastClip = (lastClip + change) % max;

            //Debug.Log(lastClip);
            return clips[lastClip];
        }

        public float RandomPitch
        {
            get
            {
                float range = maxPitch - minPitch;
                return minPitch + Random.value * range;
            }
        }

        public float RandomVolume
        {
            get
            {
                float range = maxVolume - minVolume;
                return minVolume + Random.value * range;
            }
        }
    }

}