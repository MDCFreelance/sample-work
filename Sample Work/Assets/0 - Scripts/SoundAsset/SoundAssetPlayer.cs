﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

namespace MCWork
{
    /// <summary>
    /// Player object which handles playing SoundAssets
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class SoundAssetPlayer : MonoBehaviour
    {
        [SerializeField]
        private SoundAsset asset;

        private Transform instancesRoot;
        private float editorVolume = 0f;
        private Coroutine fadeAndStopRoutine = null;

        public AudioSource Source { get; private set; }


        public SoundAsset Asset
        {
            get
            {
                return asset;
            }
        }


        public bool IsPlaying
        {
            get
            {
                if (Source == null)
                    return false;
                return Source.isPlaying;
            }
        }


        public float Length
        {
            get
            {
                if (Source.clip == null)
                    return 0f;
                return Source.clip.length;
            }
        }


        /// <summary>
        /// Grab references and cache editor volume. (fades alter the source volume and on clone we need the designer set values.
        /// </summary>
        private void Awake()
        {
            Source = GetComponent<AudioSource>();
            editorVolume = Source.volume;
        }


        /// <summary>
        /// Begin playback of a sound asset (randomized based on the available clips in the asset)
        /// </summary>
        /// <param name="asset">The asset to play</param>
        /// <param name="destroyInstances">Should any created instances be destroyed?</param>
        /// <returns>Null if this instance plays the asset directly. If a cloned player was created, it is returned instead.</returns>
        public SoundAssetPlayer Play(SoundAsset asset = null, bool destroyInstances = true)
        {
            SoundAssetPlayer target = this;

            bool childUsed = false;
            if (IsPlaying)
            {
                target = CreatePlayerInstance();
                childUsed = true;
            }

            if (asset != null)
                target.asset = asset;

            target.InternalPlay();

            if (childUsed)
            {
                if (destroyInstances)
                {
                    Destroy(target.gameObject, target.Length);
                }
                return target;
            }
            return null;
        }


        /// <summary>
        /// Kick off the clip on the actual audio source.
        /// </summary>
        private void InternalPlay()
        {
            Source.Stop();
            Source.clip = this.asset.GetRandom();
            Source.Play();
            Source.pitch = this.asset.RandomPitch;
            Source.volume = Mathf.Min(1f, this.asset.RandomVolume);
        }


        /// <summary>
        /// Instantiate a duplicate of this player to play a play another channel of the sound asset audio.
        /// </summary>
        /// <returns>A new player instance</returns>
        public SoundAssetPlayer CreatePlayerInstance()
        {
            //Make sure instance root is established.
            SetUpInstanceRoot();
            //Detach instance root so we don't clone it.
            instancesRoot.parent = null;
            //Duplicate and bind to instances root
            var instance = Instantiate<SoundAssetPlayer>(this);
            instance.transform.parent = instancesRoot;
            instance.transform.localPosition = Vector3.zero;
            instance.transform.localRotation = Quaternion.identity;
            //Reattach instance root.
            instancesRoot.parent = transform;

            return instance;
        }


        /// <summary>
        /// We can end up spawning many child objects to handle repeated Play calls, we set up a root child to contain them.
        /// </summary>
        private void SetUpInstanceRoot()
        {
            if (instancesRoot != null)
                return;
            instancesRoot = new GameObject("Instances Root").transform;
            instancesRoot.transform.SetParent(transform, false);
            instancesRoot.transform.localPosition = Vector3.zero;
            instancesRoot.transform.localEulerAngles = Vector3.zero;
        }

        /// <summary>
        /// Was used in a project which had standardize unity message naming conventions.
        /// </summary>
        public void TurnOn()
        {
            Play();
        }


        /// <summary>
        /// Was used in a project which had standardize unity message naming conventions.
        /// </summary>
        public void TurnOff()
        {
            TurnOff(0f);
        }


        /// <summary>
        /// Fade out the sound then stop it after a fade duration
        /// </summary>
        /// <param name="fadeDuration">Length of time in seconds to fade over.</param>
        private void FadeAndStop(float fadeDuration)
        {
            if (fadeAndStopRoutine != null)
            {
                CoroutineUtils.EndCoroutine(ref fadeAndStopRoutine);
            }

            float startVol = Source.volume;
            fadeAndStopRoutine = CoroutineUtils.BeginCoroutine(CoroutineUtils.DoInterpolation(1f, f =>
            {
                Source.volume = Mathf.Lerp(startVol, 0f, f);
                return true;
            },
            () =>
            {
                Source.volume = 0f;
                Source.Stop();
            }));
        }

        /// <summary>
        /// Extended message interface tied into fade/stop functionality
        /// </summary>
        /// <param name="fadeDuration">Length of fade out before "turn off"</param>
        public void TurnOff(float fadeDuration)
        {
            if (Mathf.Approximately(0f, fadeDuration))
            {
                Source.Stop();
            }
            else
            {
                FadeAndStop(fadeDuration);
            }
        }
    }
}